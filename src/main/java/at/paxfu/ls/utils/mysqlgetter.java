package at.paxfu.ls.utils;

import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class mysqlgetter {

    public static String getPREFIX() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_messages` WHERE `msgkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "prefix");
            }
        });
        try {
            prefix.next();
            return prefix.getString("message");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getNAME() {
        ResultSet name = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_messages` WHERE `msgkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "server_name");
            }
        });
        try {
            name.next();
            return name.getString("message");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getTitleLine2() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_messages` WHERE `msgkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "title_snd_line");
            }
        });
        try {
            prefix.next();
            return prefix.getString("message");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getNoPermMSG() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_messages` WHERE `msgkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "msg_noperm");
            }
        });
        try {
            prefix.next();
            return prefix.getString("message");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getJoinMSG() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_messages` WHERE `msgkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "msg_join");
            }
        });
        try {
            prefix.next();
            return prefix.getString("message");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getQuitMSG() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_messages` WHERE `msgkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "msg_quit");
            }
        });
        try {
            prefix.next();
            return prefix.getString("message");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getDefaultGameMode() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_config` WHERE `configkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "default_gamemode");
            }
        });
        try {
            prefix.next();
            return prefix.getString("config");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static Integer ifTitleEnabled() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_settings` WHERE `setting` = ?", new HashMap<Integer, String>(){
            {
                put(1, "enable_title");
            }
        });
        try {
            prefix.next();
            return prefix.getInt("enable");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static Integer ifJoinMSGEnabled() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_settings` WHERE `setting` = ?", new HashMap<Integer, String>(){
            {
                put(1, "enable_join_msg");
            }
        });
        try {
            prefix.next();
            return prefix.getInt("enable");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static Integer ifQuitMSGEnabled() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_settings` WHERE `setting` = ?", new HashMap<Integer, String>(){
            {
                put(1, "enable_quit_msg");
            }
        });
        try {
            prefix.next();
            return prefix.getInt("enable");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static Integer ifSetHotbarEnabled() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_settings` WHERE `setting` = ?", new HashMap<Integer, String>(){
            {
                put(1, "set_hotbar_on_join");
            }
        });
        try {
            prefix.next();
            return prefix.getInt("enable");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static Integer ifScoreboardEnabled() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_settings` WHERE `setting` = ?", new HashMap<Integer, String>(){
            {
                put(1, "enable_scoreboard");
            }
        });
        try {
            prefix.next();
            return prefix.getInt("enable");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getScoreboardName() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_config` WHERE `configkey` = ?", new HashMap<Integer, String>(){
            {
                put(1, "scoreboard_name");
            }
        });
        try {
            prefix.next();
            return prefix.getString("config");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

}
