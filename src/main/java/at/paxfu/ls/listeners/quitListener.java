package at.paxfu.ls.listeners;

import at.paxfu.ls.api.mysqlAPI;
import at.paxfu.ls.utils.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import at.paxfu.ls.utils.mysqlgetter;
import at.paxfu.ls.api.statusAPI;

import java.sql.ResultSet;
import java.util.HashMap;

public class quitListener implements Listener {

    Integer quit_msg = mysqlgetter.ifQuitMSGEnabled();

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        event.setQuitMessage("");

        if(quit_msg.equals(1)) {
            Bukkit.broadcastMessage(mysqlgetter.getQuitMSG() + player.getName());
        }

        //ONLINE PLAYER COUNT
        ResultSet prefix = MySQL.getInstance().executeQuery("INSERT INTO `lobby_info`(`value1`, `value2`) VALUES (?,?)", new HashMap<Integer, String>() {
            {
                put(1, "onlineplayers");
                put(2, String.valueOf(Bukkit.getOnlinePlayers().size() - 1));
            }
        });

        statusAPI.setOfflineState(player.getUniqueId());
    }
}
