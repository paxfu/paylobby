package at.paxfu.ls.listeners;

import at.paxfu.ls.LobbySystem;
import at.paxfu.ls.api.mysqlAPI;
import at.paxfu.ls.utils.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import at.paxfu.ls.managers.lobbyInventory;
import at.paxfu.ls.utils.mysqlgetter;
import at.paxfu.ls.utils.scoreboardgetter;
import at.paxfu.ls.utils.scoreboard;
import at.paxfu.ls.api.statusAPI;

import java.sql.ResultSet;
import java.util.HashMap;

public class joinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {



        Integer title = mysqlgetter.ifTitleEnabled();
        Integer join_msg = mysqlgetter.ifJoinMSGEnabled();
        Integer set_inv = mysqlgetter.ifSetHotbarEnabled();

        Player player = event.getPlayer();

        //CREATE USER IN DATABASE
        if(!player.hasPlayedBefore()) {
            ResultSet set = MySQL.getInstance().executeQuery("INSERT INTO `lobby_users`(`username`, `uuid`, `online`) VALUES (?,?,?)", new HashMap<Integer, String>() {
                {
                    put(1, player.getName());
                    put(2, String.valueOf(player.getUniqueId()));
                    put(3, "yes");
                }
            });
        }

        player.getInventory().clear();
        event.setJoinMessage(null);
        player.setHealth(20);
        player.setFoodLevel(20);

        double x = LobbySystem.locations.getDouble("Spawn.X");
        double y = LobbySystem.locations.getDouble("Spawn.Y");
        double z = LobbySystem.locations.getDouble("Spawn.Z");
        float yaw = (float) LobbySystem.locations.getDouble("Spawn.YAW");
        float pitch = (float) LobbySystem.locations.getDouble("Spawn.PITCH");
        World world = Bukkit.getWorld(LobbySystem.locations.getString("Spawn.WORLD"));

        Location location = new Location(world, x, y, z, yaw, pitch);
        player.teleport(location);

        if(mysqlgetter.ifScoreboardEnabled().equals(1)) {
            scoreboard.setScoreboard(player);
        }







        //Title
        if(title.equals(1)) {
            player.sendTitle(mysqlgetter.getNAME(), mysqlgetter.getTitleLine2());
        }
        //JoinMSG
        if(join_msg.equals(1)) {
            Bukkit.broadcastMessage(mysqlgetter.getJoinMSG() + player.getDisplayName());
        }
        //Default Gamemode
        if(mysqlgetter.getDefaultGameMode().equalsIgnoreCase("survival")) {
            player.setGameMode(GameMode.SURVIVAL);
        }else if(mysqlgetter.getDefaultGameMode().equalsIgnoreCase("creative")) {
            player.setGameMode(GameMode.CREATIVE);
        }else if(mysqlgetter.getDefaultGameMode().equalsIgnoreCase("adventure")) {
            player.setGameMode(GameMode.ADVENTURE);
        }else if(mysqlgetter.getDefaultGameMode().equalsIgnoreCase("spectator")) {
            player.setGameMode(GameMode.SPECTATOR);
        }
        //Set Hotbar
        if(set_inv.equals(1)) {
            lobbyInventory.setInventory(player);
        }



        //ONLINE PLAYER COUNT
        ResultSet prefix = MySQL.getInstance().executeQuery("INSERT INTO `lobby_info`(`value1`, `value2`) VALUES (?,?)", new HashMap<Integer, String>() {
            {
                put(1, "onlineplayers");
                put(2, String.valueOf(Bukkit.getOnlinePlayers().size()));
            }
        });

        //SET STATUS
        statusAPI.setOnlineState(player.getUniqueId());

    }
}
