package at.paxfu.ls.api;

import at.paxfu.ls.utils.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class statusAPI {

    public static void setOnlineState(UUID uuid) {
        ResultSet prefix = MySQL.getInstance().executeQuery("UPDATE `lobby_users` SET `online`= ? WHERE `uuid` = ?", new HashMap<Integer, String>() {
            {
                put(1, "yes");
                put(2, String.valueOf(uuid));
            }
        });
    }

    public static void setOfflineState(UUID uuid) {
        ResultSet prefix = MySQL.getInstance().executeQuery("UPDATE `lobby_users` SET `online`= ? WHERE `uuid` = ?", new HashMap<Integer, String>() {
            {
                put(1, "no");
                put(2, String.valueOf(uuid));
            }
        });
    }
}
