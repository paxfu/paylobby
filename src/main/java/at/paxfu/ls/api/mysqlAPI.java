package at.paxfu.ls.api;

import at.paxfu.ls.utils.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class mysqlAPI {

    public static String selectListener = "";

    public static String getListenerData() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_listeners` WHERE `listener` = ?", new HashMap<Integer, String>(){
            {
                put(1, selectListener);
            }
        });
        try {
            prefix.next();
            return prefix.getString("enabled");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

}
