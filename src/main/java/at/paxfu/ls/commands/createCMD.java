package at.paxfu.ls.commands;

import at.paxfu.ls.LobbySystem;
import at.paxfu.ls.utils.MySQL;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import at.paxfu.ls.utils.mysqlgetter;

import javax.xml.transform.Result;
import java.sql.ResultSet;
import java.util.HashMap;

public class createCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player) sender;

            //create webuser <username>
            if(player.hasPermission("lobby.create")) {
                if(args.length == 2) {
                    if(args[0].equalsIgnoreCase("webuser")) {

                        ResultSet set = MySQL.getInstance().executeQuery("INSERT INTO `lobby_webpanel_users`(`username`, `password`, `panelrank`, `mcuuid`, `firstTimeUse`) VALUES (?,?,?,?,?)", new HashMap<Integer, String>(){
                            {
                                put(1, args[1]);
                                put(2, "firstuse");
                                put(3, "user");
                                put(4, String.valueOf(player.getUniqueId()));
                                put(5, "0");
                                player.sendMessage(mysqlgetter.getPREFIX() + "Webuser successfully created!");
                            }
                        });


                    }else {
                        player.sendMessage(mysqlgetter.getPREFIX() + "Please enter §e/create webuser <USERNAME>§7!");
                    }
                }else {
                    player.sendMessage(mysqlgetter.getPREFIX()+ "Please enter §e/create webuser <USERNAME>§7!");
                }
            }else {
                player.sendMessage(mysqlgetter.getPREFIX() + mysqlgetter.getNoPermMSG());
            }
        }
        return false;
    }
}
