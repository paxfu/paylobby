package at.paxfu.ls.commands;

import at.paxfu.ls.LobbySystem;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import at.paxfu.ls.utils.mysqlgetter;

import java.io.IOException;

public class setspawnCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if(player.hasPermission("lobby.setspawn")) {
            if(args.length == 1) {
                if(args[0].equalsIgnoreCase("spawn")) {
                    LobbySystem.locations.set("Spawn.X", player.getLocation().getX());
                    LobbySystem.locations.set("Spawn.Y", player.getLocation().getY());
                    LobbySystem.locations.set("Spawn.Z", player.getLocation().getZ());
                    LobbySystem.locations.set("Spawn.PITCH", player.getLocation().getPitch());
                    LobbySystem.locations.set("Spawn.YAW", player.getLocation().getYaw());
                    LobbySystem.locations.set("Spawn.WORLD", player.getLocation().getWorld().getName());

                    try {
                        LobbySystem.locations.save(LobbySystem.locationsf);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    player.sendMessage(mysqlgetter.getPREFIX() + "Spawn set!");
                }
            }else {
                player.sendMessage(mysqlgetter.getPREFIX() + "§cWrong alias! §7Use /setspawn spawn");
            }
        }else {
            player.sendMessage(mysqlgetter.getPREFIX() + mysqlgetter.getNoPermMSG());
        }
        return false;
    }
}
